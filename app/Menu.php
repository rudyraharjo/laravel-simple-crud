<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $primaryKey = 'id';
    
    protected $table = 'menus';
    
    protected $fillable = [
        'label', 'parent_id', 'label', 'icon', 'url'
    ];

    public function parent()
    {
        return $this->belongsTo('App\menu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\menu', 'parent_id');
    }

}
