<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
class MenuController extends Controller
{
    public function index()
    {
        // $data = Menu::orderBy('id', 'asc')->get();
        $data = Menu::with('children')->where('parent_id','=',0)->get();
        return response()->json($data);
        //return User::latest()->paginate(10);
    }

    public function store(Request $request)
    {   
        
        $this->validate($request,[
            'label' => 'required|string|max:191',
        ]);
        
        $id = 0;
        $saveMenu = Menu::create([
            'parent_id' => $request['id'] > 0 ? $request['id'] : $id,
            'label' => $request['label'],
            'url' => $request['url']
        ]);

        if($saveMenu){
            return [
                'type'=> 'success',
                'title' => 'Menu Created in successfully'
            ];
        } else{
            return [
                'type'=> 'failed',
                'title' => 'Menu Failed Created'
            ];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);
        $this->validate($request,[
            'label' => 'required|string|max:191',
        ]);

        $menu->label = $request->label;
        $menu->url = $request->url;
        $menu->save();

        return ['message' => 'Updated the menu'];
    }

    public function destroy($id)
    {
        $user = Menu::findOrFail($id);
        // delete the user
        $user->delete();
        return ['message' => 'User Deleted'];
    }
}
