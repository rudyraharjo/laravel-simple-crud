# Laravel simple crud

**How to use**

*  Clone the repository with git clone
*  Copy .env.example file to .env and edit database credentials there
*  Run composer install
*  Run php artisan key:generate
*  Run php artisan migrate
*  Run npm install
*  Run npm run dev
*  Run npm run watch to keep development mode
*  That's it - load the homepage, use local/register link for create new account
